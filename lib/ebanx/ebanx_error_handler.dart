import 'package:spring_response_service/form_error_response.dart';
import 'package:spring_response_service/spring_response.dart';

import 'responses/ebanx_credit_card_token_response.dart';

class EbanxErrorHandler {

  SpringResponse<EbanxCreditCardTokenResponse> handleError(EbanxCreditCardTokenResponse response) {
    return SpringResponse(
      errors: [ FormErrorResponse(defaultMessage: "Dados do cartão inválido") ]
    );


//    if(response.statusCode == EbanxResponseStatusCode.cardNumberIsInvalid) {
//      return SpringResponse(
//        errors: [ FormErrorResponse(defaultMessage: "Número do cartão inválido") ]
//      );
//    }
//
//    return SpringResponse(
//        errors: [ FormErrorResponse(defaultMessage: "Houve um erro") ]
//    );

  }

}