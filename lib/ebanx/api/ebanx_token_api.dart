import 'package:dio/dio.dart';

class EbanxTokenApi {

  final Dio _client;

  EbanxTokenApi(this._client);

  Future<Response> getToken(Map<String, dynamic> request) async {
    return _client.post("https://staging.ebanx.com.br/ws/token", //https://api.ebanx.com.br/ws/token
      data: request
    );
  }
  
}