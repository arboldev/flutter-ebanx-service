import 'package:dio/dio.dart';
import 'package:ebanx_service/ebanx/requests/ebanx_credit_card_token_request.dart';
import 'package:ebanx_service/ebanx/responses/ebanx_credit_card_token_response.dart';
import 'package:spring_response_service/dio_error_handler.dart';
import 'package:spring_response_service/spring_response.dart';

import '../ebanx_error_handler.dart';
import '../ebanx_rest_services.dart';

class EbanxCreditCardTokenDataSource {

  final restService = EbanxRestService();

  Future<SpringResponse<EbanxCreditCardTokenResponse>> getToken(EbanxCreditCardTokenRequest request) async {
    try {
      final response = await restService.tokenApi.getToken(request.toJson());
      final content = EbanxCreditCardTokenResponse.fromJson(response.data);

      if(content.isSuccess) {
        return SpringResponse(
          isSuccessful: true,
          body: content
        );
      } else {
        return EbanxErrorHandler().handleError(content);
      }
    } on DioError catch(e) {

      return SpringErrorHandler().handleError(e);
    }
  }

}