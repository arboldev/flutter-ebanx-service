import '../ebanx_credit_card.dart';

class EbanxCreditCardTokenRequest {

  final String integrationKey;
  final String publicIntegrationKey;
  final String paymentTypeCode;
  final String country;
  final EbanxCreditCard creditCard;

  EbanxCreditCardTokenRequest({
    this.publicIntegrationKey,
    this.integrationKey,
    this.paymentTypeCode,
    this.country,
    this.creditCard
  });

  Map<String, dynamic> toJson() => {
    "public_integration_key": this.publicIntegrationKey,
    "integration_key": this.integrationKey,
    "country": this.country,
    "creditcard": this.creditCard.toJson(),
    "payment_type_code": paymentTypeCode
  };

}