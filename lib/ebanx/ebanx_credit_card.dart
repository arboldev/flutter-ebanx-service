class EbanxCreditCard {

  final String cardName;
  final String cardNumber;
  final String cardDueDate;
  final String cardCVV;

  EbanxCreditCard({
    this.cardName,
    this.cardNumber,
    this.cardDueDate,
    this.cardCVV
  });

   Map<String, dynamic> toJson() => {
    "card_number": this.cardNumber,
    "card_name": this.cardName,
    "card_due_date": this.cardDueDate,
    "card_cvv": this.cardCVV,
  };
}