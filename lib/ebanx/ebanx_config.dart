class EbanxConfig {

  static EbanxConfig _instance;

  EbanxConfig._internalConstructor();

  factory EbanxConfig(){
    if(_instance == null) {
      _instance = EbanxConfig._internalConstructor();
    }
    return _instance;
  }

  String integrationToken = "b4997ba427f9577c9a779e11df3815a7a626e8648ae7e72a1a25d8e9c4978ef8aad81fc33fddabb88d2a62ce46a835de3ccf";
//  String integrationToken = "9768e32bbe277fe76f17f3a28af166a443df35aeb11daeefc4e3614634d7ee44a815f41c86961ca28daed47366b8f7d54ce6";
}