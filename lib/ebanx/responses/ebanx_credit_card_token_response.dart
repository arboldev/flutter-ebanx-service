

class EbanxCreditCardTokenResponse {

  static const _SUCCESS = "SUCCESS";
  static const _ERROR = "ERROR";

  final String status;
  final String paymentTypeCode;
  final String token;
  final String maskedCardNumber;

  final String statusCode;
  final String statusMessage;

  EbanxCreditCardTokenResponse({
    this.status,
    this.paymentTypeCode,
    this.token,
    this.maskedCardNumber,
    this.statusCode,
    this.statusMessage
  });

  bool get isSuccess => status == _SUCCESS;

  factory EbanxCreditCardTokenResponse.fromJson(Map<String, dynamic> json) {
    if(json['status'] == _SUCCESS){
      return EbanxCreditCardTokenResponse.fromSuccessJson(json);
    } else {
      return EbanxCreditCardTokenResponse.fromErrorJson(json);
    }
  }

  factory EbanxCreditCardTokenResponse.fromSuccessJson(Map<String, dynamic> json) {
    return EbanxCreditCardTokenResponse(
      status: json['status'],
      paymentTypeCode: json['payment_type_code'],
      token: json['token'],
      maskedCardNumber: json['masked_card_number'],
    );
  }

  factory EbanxCreditCardTokenResponse.fromErrorJson(Map<String, dynamic> json) {
    return EbanxCreditCardTokenResponse(
      status: json['status'],
      statusCode: json['status_code'],
      statusMessage: json['status_message']
    );
  }

}