import 'package:dio/dio.dart';

import 'api/ebanx_token_api.dart';

class EbanxRestService {

  static Dio _createApi() {

    final options = new BaseOptions(
      connectTimeout: 10000,
      receiveTimeout: 10000,
    );

    return Dio(options);
  }

  EbanxTokenApi get tokenApi => EbanxTokenApi(_createApi());


}